//setting the height of left-nav
function setHeight() {
    var heigth = $(window).height();
    $('.left-nav').css('min-height', heigth + 'px');
    $('.box1').css('min-height', heigth - 100 + 'px');
    $('.box2').css('min-height', heigth - 100 + 'px');
    var b1h =  $('.box1').height();
    var b2h =  $('.box2').height();
    if(b1h>b2h){
         $('.box2').css('min-height', b1h + 'px');
    }
    if(b1h<b2h){
         $('.box1').css('min-height', b2h + 'px');
    }
}
;
setHeight();
$(window).resize(function() {
    setHeight();
});
$(window).scroll(function() {
    setHeight();
});

//colapsible navigation
$('.colapsible').unbind().each(function() {
    $(this).on('click', function() {
        if ($(this).parent().hasClass('opened')) {
            $(this).parent().removeClass('opened');
            $(this).parent().addClass('closed');
        } else if ($(this).parent().hasClass('closed')) {
            $(this).parent().removeClass('closed');
            $(this).parent().addClass('opened');
        }
    });
});
//settings hover
$(document).ready(function() {
    $('.settings').hover().css('cursor', 'pointer');
    $('.s_edit').hover().css('cursor', 'pointer');
    $('.settings').unbind().each(function() {
        $(this).on('click', function() {
            $(this).next().toggle();
            $('.settings_body').not($(this).next()).hide();
        });
    });

});
//document.getElementById("uploadBtn").onchange = function () {
//    document.getElementById("uploadFile").value = this.value;
//};





